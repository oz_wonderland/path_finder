import React, { useRef, useState } from 'react';
import { dijkstra, DijkstraNode } from '../algorithms/dijkstra';
import FunctionBasedNode from './node/functionBasedNode';

const NB_ROWS = 20;
const NB_COLS = 50;
const START_NODE_ROW = 10;
const START_NODE_COL = 10;
const FINISH_NODE_ROW = 15;
const FINISH_NODE_COL = 40;

const FunctionBasedVisualizer = () => {
  const init = () => {
    const items: DijkstraNode[][] = [];
    for (let row = 0; row < NB_ROWS; row++) {
      const currentRow: DijkstraNode[] = [];
      for (let col = 0; col < NB_COLS; col++) {
        currentRow.push({
          isStart: (col === START_NODE_COL && row === START_NODE_ROW),
          isFinish: (col === FINISH_NODE_COL && row === FINISH_NODE_ROW),
          isWall: false,
          isVisited: false,
          row: row,
          column: col,
          previousNode: undefined,
          distance: Infinity
        });
      }
      items.push(currentRow);
    }
    return items;
  };
  const [grid, setGrid] = useState(init());
  const nodeRefs = useRef<HTMLDivElement[][]>([]);

  const onMouseDown = (row: number, column: number) => {
    computeGridWithWall(row, column);
  };
  const computeGridWithWall = (row: number, column: number) => {
    const newGrid = grid.slice();
    const node = newGrid[row][column];
    const { isStart, isFinish, isWall } = node;
    const newNode: DijkstraNode = {
      isStart: isStart,
      isFinish: isFinish,
      isWall: !isWall,
      isVisited: false,
      row: row,
      column: column,
      previousNode: undefined,
      distance: Infinity
    };

    newGrid[row][column] = newNode;
    setGrid(newGrid);
  };
  const animateDijkstra = (visitedNodesInOrder: DijkstraNode[], shortestPath: DijkstraNode[]) => {
    for (let i = 0; i <= visitedNodesInOrder.length; i++) {
      if (i === visitedNodesInOrder.length) {
        setTimeout(() => {
          animateShortestPath(shortestPath);
        }, 10 * i);
        return;
      }
      setTimeout(() => {
        const node = visitedNodesInOrder[i];
        const nodeRef = nodeRefs.current[node.row][node.column];

        nodeRef.className = 'node node-visited';
      }, 10 * i);
    }
  };

  const animateShortestPath = (nodesInShortestPathOrder: DijkstraNode[]) => {
    if (nodesInShortestPathOrder.length === 1) {
      return;
    }
    for (let i = 0; i < nodesInShortestPathOrder.length; i++) {
      setTimeout(() => {
        const node = nodesInShortestPathOrder[i];
        const nodeRef = nodeRefs.current[node.row][node.column];

        nodeRef.className = 'node node-shortest-path';
      }, 50 * i);
    }
  };

  const getShortestPath = (finish: DijkstraNode): DijkstraNode[] => {
    const shortestPath: DijkstraNode[] = [];
    let currentNode = finish;

    while (currentNode !== undefined) {
      shortestPath.unshift(currentNode);
      if (currentNode.previousNode === undefined) {
        return shortestPath;
      }
      currentNode = currentNode.previousNode!;
    }
    return shortestPath;
  };

  const visualizeDijkstra = () => {
    const startNode = grid[START_NODE_ROW][START_NODE_COL];
    const finishNode = grid[FINISH_NODE_ROW][FINISH_NODE_COL];
    const visitedNodesInOrder = dijkstra(grid, startNode, finishNode);
    const shortestPath = getShortestPath(finishNode);

    animateDijkstra(visitedNodesInOrder, shortestPath);
  };

  return (
    <div>
      <p>
        <button onClick={ () => visualizeDijkstra() }>run djikstra</button>
      </p>
      {grid.map((row, rowIdx) => {
        nodeRefs.current[rowIdx] = [];
        return (
          <div key={rowIdx}>
            {row.map((item, colIdx) => {
              const { isStart, isFinish } = item;

              return (
                <FunctionBasedNode
                  isStartValue={isStart}
                  isFinishValue={isFinish}
                  column={colIdx}
                  row={rowIdx}
                  onMouseDown={(rowIdx, colIdx) => onMouseDown(rowIdx, colIdx)}
                  key={colIdx + '-' + rowIdx}
                  ref={(element) => { nodeRefs.current[rowIdx][colIdx] = element!; }}/>
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

export default FunctionBasedVisualizer;