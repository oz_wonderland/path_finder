import React, { forwardRef, useState } from 'react';
import './Node.css';

interface Props {
  isStartValue: boolean;
  isFinishValue: boolean;
  row: number;
  column: number;
  onMouseDown: (row: number, column: number) => void;
}

const FunctionBasedNode = forwardRef<HTMLDivElement, Props>(
  ({ row, column, onMouseDown, isStartValue, isFinishValue }, ref) => {
    const [isStart] = useState(isStartValue);
    const [isWall, setIsWall] = useState(false);
    const [isFinish] = useState(isFinishValue);
    const [isVisited] = useState(false);

    const extractClassName = (): string =>
      isStart ? 'node-start'
        : isFinish ? 'node-finish'
          : isWall ? 'node-wall'
            : isVisited ? 'node-visited'
              : '';

    return (
      <div
        ref={ref}
        className={`node ${extractClassName()}`}
        id={`node-${row}-${column}`}
        onMouseDown={() => {
        // start and end points cannot be walls
          if (isStart || isFinish) return;

          onMouseDown(row, column);
          setIsWall(!isWall);
        }}
      />
    );
  });

FunctionBasedNode.displayName = 'FunctionBasedNode';

export default FunctionBasedNode;