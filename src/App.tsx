import React from 'react';
import './App.css';
import FunctionBasedVisualizer from './visualizer/FunctionBasedVisualizer';

function App () {
  return (
    <div className="App">
      <FunctionBasedVisualizer></FunctionBasedVisualizer>
    </div>
  );
}

export default App;