export type DijkstraNode = {
  isStart: boolean;
  isFinish: boolean;
  isWall: boolean;
  isVisited: boolean;
  row: number;
  column: number;
  previousNode?: DijkstraNode;
  distance: number;
};

export function dijkstra (grid: DijkstraNode[][], start: DijkstraNode, finish: DijkstraNode): DijkstraNode[] {
  const visitedNodesInOrder: DijkstraNode[] = [];
  const unvisitedNodes = getAllNodes(grid);

  start.distance = 0;
  while (unvisitedNodes.length) {
    sortNodesByDistance(unvisitedNodes);
    const closetNode = unvisitedNodes.shift();

    if (closetNode === undefined) return visitedNodesInOrder;
    if (closetNode.isWall) continue;
    if (closetNode?.distance === Infinity) return visitedNodesInOrder;
    closetNode.isVisited = true;
    visitedNodesInOrder.push(closetNode);
    if (closetNode === finish) return visitedNodesInOrder;
    updateUnvisiteNeighbors(closetNode, grid);
  }
  return visitedNodesInOrder;
};

function sortNodesByDistance (nodes: DijkstraNode[]): void {
  nodes.sort((a: DijkstraNode, b: DijkstraNode) => a.distance - b.distance);
}

function updateUnvisiteNeighbors (closetNode: DijkstraNode, grid: DijkstraNode[][]): void {
  const unvisitedNodes = getUnvisitedNeighbors(closetNode, grid);

  unvisitedNodes.forEach(node => {
    node.distance = closetNode.distance + 1;
    node.previousNode = closetNode;
  });
}

function getUnvisitedNeighbors (node: DijkstraNode, grid: DijkstraNode[][]): DijkstraNode[] {
  const neighbors: DijkstraNode[] = [];
  const { column, row } = node;

  if (row > 0) neighbors.push(grid[row - 1][column]);
  if (row < grid.length - 1) neighbors.push(grid[row + 1][column]);
  if (column > 0) neighbors.push(grid[row][column - 1]);
  if (column < grid[0].length - 1) neighbors.push(grid[row][column + 1]);
  return neighbors.filter(node => !node.isVisited);
}

function getAllNodes (grid: DijkstraNode[][]): DijkstraNode[] {
  const nodes: DijkstraNode[] = [];

  for (const row of grid) {
    for (const node of row) {
      nodes.push(node);
    }
  }
  return nodes;
}